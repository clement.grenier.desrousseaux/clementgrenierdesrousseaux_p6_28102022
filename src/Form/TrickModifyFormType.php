<?php

namespace App\Form;

use App\Entity\Tags;
use App\Entity\Trick;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrickModifyFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de la figure'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de la figure'
            ])
            ->add('tags', EntityType::class, ['class' => Tags::class, 'choice_label' => 'tagName', 'multiple' => true], [
                'label' => 'Tags'
            ])
            ->add('video', TextareaType::class, [
                'required' => false,
                'label' => 'Vidéo',
                'help' => 'Coller les balises iframe, séparer les vidéos par un retour à la ligne'
            ])
            ->
            add('images', FileType::class, [
                'label' => "Images",
                'multiple' => true,
                'mapped' => false,
                'required' => false,

            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Modifier le trick'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Trick::class,
        ]);
    }
}
