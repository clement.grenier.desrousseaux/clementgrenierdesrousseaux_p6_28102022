<?php

namespace App\Form;

use App\Entity\Tags;
use App\Entity\Trick;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class TrickCreateFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class, [
                'required' => true,
                'label' => 'Nom du trick'
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'Description du trick',
            ])
            ->add('tags', EntityType::class, [
                'class' => Tags::class,
                'choice_label' => 'tagName',
                'multiple' => true,
                'label' => 'Tags',
            ])
            ->add('video', TextareaType::class, [
                'required' => false,
                'label' => 'Video',
                'help' => 'Coller les balises iframe, séparer les vidéos par un retour à la ligne'
            ])
            //change name button
            ->add('images', FileType::class, [
                'label' => "Images",
                'multiple' => true,
                'mapped' => false,
                'required' => true,

            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Créer le trick'
            ]);

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Trick::class,
        ]);
    }
}
