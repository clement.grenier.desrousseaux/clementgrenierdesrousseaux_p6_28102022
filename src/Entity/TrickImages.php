<?php

namespace App\Entity;

use App\Repository\TrickImagesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TrickImagesRepository::class)]
class TrickImages
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\Trick', inversedBy: 'images')]
    #[ORM\JoinColumn(nullable: false)]
    private $trick;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTrick(): ?Trick
    {
        return $this->trick;
    }

    public function setTrick(?Trick $annonces): self
    {
        $this->trick = $annonces;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
