<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Tags;
use App\Entity\Trick;
use App\Entity\TrickImages;
use App\Entity\User;
use App\Entity\UserImage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;

class TricksFixtures extends Fixture implements FixtureGroupInterface
{
    private $container;

    public function load(ObjectManager $manager): void
    {
        $userImageAdmin = new UserImage();
        $userImageAdmin->setName('81ed4858fe795952810d7ed21ec10283.webp');
        $manager->persist($userImageAdmin);

        $userImage1 = new UserImage();
        $userImage1->setName('1175125f961f08bc2609ce3ce1ccbf1b.webp');
        $manager->persist($userImage1);

        $userImage2 = new UserImage();
        $userImage2->setName('53afcf1e7d3abbd832bf25f800b974e7.webp');
        $manager->persist($userImage2);

        $userAdmin = new User();
        $userAdmin->setEmail('clement@test.admin');
//        $encodedPasswordAdmin = $encoder->encodePassword($userAdmin, 'test1234');
        $userAdmin->setPassword($userAdmin->hashPassword('test1234'));
        $userAdmin->setRoles(['ROLE_ADMIN']);
        $userAdmin->setImage($userImageAdmin);
        $userAdmin->setImage($userImageAdmin);
        $userAdmin->setUsername('Admin');
        $userAdmin->setIsVerified(true);
        $manager->persist($userAdmin);

        $userUser1 = new User();
        $userUser1->setEmail('david@test.user');
//        $encodedPasswordUser = $encoder->encodePassword($userAdmin, 'test1234');
        $userUser1->setPassword($userUser1->hashPassword('test1234'));
        $userUser1->setRoles(['ROLE_USER']);
        $userUser1->setImage($userImage1);
        $userUser1->setUsername('David');
        $userUser1->setIsVerified(true);
        $manager->persist($userUser1);


        $userUser2 = new User();
        $userUser2->setEmail('laeticia@test.user');
//        $encodedPasswordUser = $encoder->encodePassword($userAdmin, 'test1234');
        $userUser2->setPassword($userUser2->hashPassword('test1234'));
        $userUser2->setRoles(['ROLE_USER']);
        $userUser2->setImage($userImage2);
        $userUser2->setUsername('Laeticia');
        $userUser2->setIsVerified(true);
        $manager->persist($userUser2);

        $tag1 = new Tags();
        $tag1->setTagName('Saut');
        $manager->persist($tag1);

        $tag2 = new Tags();
        $tag2->setTagName('Neige');
        $manager->persist($tag2);

        $tag3 = new Tags();
        $tag3->setTagName('Montagne');
        $manager->persist($tag3);

        $tag4 = new Tags();
        $tag4->setTagName('Compétition');
        $manager->persist($tag4);

        $tag5 = new Tags();
        $tag5->setTagName('Figure');
        $manager->persist($tag5);

        $tag6 = new Tags();
        $tag6->setTagName('Dangereux');
        $manager->persist($tag6);


        $trick1 = new Trick();
        $trick1->addTag($tag1);
        $trick1->addTag($tag4);
        $trick1->addTag($tag2);
        $trick1->setUser($userUser1);
        $trick1->setName('Backflip');
        $trick1->setSlug('backflip');
        $trick1->setDescription("Le Backflip est l'une des figures les plus emblématiques du snowboard, et on ne pouvait pas l’exclure de notre liste ! Une fois que tu as quitté le kicker et que tu as assez de hauteur et d'élan, il faut jeter ton poids en arrière pour faire une rotation verticale - ou un Flip - pour avoir la tête en bas et les jambes en haut, tout en relâcher tes jambes au bon moment pour atterrir. Tu as probablement eu quelques accidents désagréables en le perfectionnant, mais cela en vaut la peine lorsque tu vois les visages et les acclamations de tes spectateurs. 

Le conseil des pros : Les snowboardeurs professionnels s’entrainent à faire des Backflips sur des trampolines en été, pour les aider à faire les bons mouvements au bon moment au snowpark en hiver.");
        $trick1->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick1);


        $trick2 = new Trick();
        $trick2->addTag($tag3);
        $trick2->addTag($tag4);
        $trick2->addTag($tag6);
        $trick2->setUser($userUser2);
        $trick2->setName('Indy Grab');
        $trick2->setSlug('indy-grab');
        $trick2->setDescription("Si tu t’es entrainé à faire des sauts au snowpark et que tu commences à prendre de la hauteur et de la confiance sur les atterrissages, tu peux commencer à ajouter des figures créatives, comme le Indy Grab, pour donner plus de styles à tes sauts. 

Le terme ‘’Indy’’ n’a pas d’explication particulière, mais un ‘’Grab’’ en anglais veut dire saisir/attraper quelque chose avec sa main. Dans le contexte du snowboard, tu veux essayer de saisir le bord de ta planche avec ta main pendant que tu voles dans les airs. 

Pour faire un Indy Grab, il faut décoller d'un kicker et utiliser la technique du Ollie. Le Ollie te permettra de prendre plus de hauteur au départ du saut et de suffisamment plier tes jambes pour ensuite attraper ta planche en l’air. Une fois en l’air, il faut saisir le bord de ta planche entre les deux fixations avec ta main arrière. La durée du Grab dépendra de la hauteur que tu prendras sur le saut. Mais surtout n’oublie pas de relâcher la planche avant l’atterrissage, car sinon tu risques de te faire très mal aux doigts ! Tout est dans le relâchement en douceur de la prise Indy, qui te permettra de lâcher prise à temps pour atterrir comme un patron. 

Le truc en plus : Pour impressionner les autres snowboardeurs et augmenter la difficulté de tes sauts, tu peux maintenant combiner les rotations 180 ou 360 avec un Indy Grab dans le même saut.");
        $trick2->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick2);


        $trick3 = new Trick();
        $trick3->addTag($tag1);
        $trick3->addTag($tag4);
        $trick3->addTag($tag6);
        $trick3->addTag($tag2);
        $trick3->setUser($userUser2);
        $trick3->setName('Jib');
        $trick3->setSlug('jib');
        $trick3->setDescription("Le Jib est l'une des figures de base à apprendre quand tu te lances dans le park, car il est utilisé sur la plupart des figures freestyle. Le jibbing consiste à chevaucher, sauter ou glisser sur tout ce qui n'est pas une surface piquée, comme les rails, les bancs ou une bûche. Comme pour le Butter, il faut éviter d’utiliser les lames/bords sur les rails et les boxs, car elles ne t’aideront pas à t’arrêter si jamais tu perds l’équilibre.");
        $trick3->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick3);


        $trick4 = new Trick();
        $trick4->addTag($tag1);
        $trick4->addTag($tag4);
        $trick4->addTag($tag2);
        $trick4->addTag($tag3);
        $trick4->setUser($userUser1);
        $trick4->setName('Nose Press');
        $trick4->setSlug('nose-press');
        $trick4->setDescription("Avant de faire une explication détaillée sur le Nose Press et le Tail Press, il faut d’abord connaitre les termes anglais du ‘’Nose’’ et du ‘’Tail’’, qui se traduisent par ‘’le nez’’ et ‘’la queue’’ en français. Sur un snowboard, on appelle le devant de la planche le Nose/nez et l’arrière de la planche le Tail/queue. Que tu sois en Regular, en Goofy ou en Switch, le nez sera toujours pointé dans la direction de la piste.

Revenons maintenant à notre explication. Un Tail Press consiste à simplement transférer ton poids sur ta jambe arrière tout en soulevant légèrement ta jambe avant, ce qui soulèvera l'avant de la planche. Le Nose Press est l'inverse. Le poids est transféré sur la jambe avant et l'arrière de la planche se plie vers le haut. Il s'agit d'une astuce classique, mais basique, qui donne à n'importe quel planchiste un look \"steezy\". À noter que, si tu as une planche flexible, tu devrais avoir plus d’effets et de hauteurs au moment de soulever ta planche !

Le conseil des pros : Commence par le faire sur un espace plat sans mouvement pour trouver le bon équilibre, et dès que tu arrives à le faire sur le plat sans bouger, tu peux ensuite le faire en mouvement. Si tu maitrises les deux figures en mouvement, tu peux essayer de le faire sur un box au snowpark.");
        $trick4->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick4);

        $trick5 = new Trick();
        $trick5->addTag($tag1);
        $trick5->addTag($tag4);
        $trick5->addTag($tag2);
        $trick5->setUser($userUser1);
        $trick5->setName('Ollie');
        $trick5->setSlug('ollie');
        $trick5->setDescription("L’Ollie est une figure de base qui consiste à sauter en l’air avec son snowboard. Pour réaliser un Ollie, il faut se placer en position de départ, c’est-à-dire avec les pieds collés l’un à l’autre, les genoux légèrement fléchis et le dos bien droit. Ensuite, il faut se mettre en position de saut, c’est-à-dire en appuyant sur les talons et en se penchant légèrement en avant. Enfin, il faut sauter en appuyant sur les talons et en gardant les genoux fléchis. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds");
        $trick5->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick5);


        $trick6 = new Trick();
        $trick6->addTag($tag1);
        $trick6->addTag($tag4);
        $trick6->addTag($tag2);
        $trick6->setUser($userUser1);
        $trick6->setName('Ollie1');
        $trick6->setSlug('ollie1');
        $trick6->setDescription("L’Ollie est une figure de base qui consiste à sauter en l’air avec son snowboard. Pour réaliser un Ollie, il faut se placer en position de départ, c’est-à-dire avec les pieds collés l’un à l’autre, les genoux légèrement fléchis et le dos bien droit. Ensuite, il faut se mettre en position de saut, c’est-à-dire en appuyant sur les talons et en se penchant légèrement en avant. Enfin, il faut sauter en appuyant sur les talons et en gardant les genoux fléchis. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds");
        $trick6->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick6);

        $trick7 = new Trick();
        $trick7->addTag($tag1);
        $trick7->addTag($tag4);
        $trick7->addTag($tag2);
        $trick7->setUser($userUser1);
        $trick7->setName('Ollie2');
        $trick7->setSlug('ollie2');
        $trick7->setDescription("L’Ollie est une figure de base qui consiste à sauter en l’air avec son snowboard. Pour réaliser un Ollie, il faut se placer en position de départ, c’est-à-dire avec les pieds collés l’un à l’autre, les genoux légèrement fléchis et le dos bien droit. Ensuite, il faut se mettre en position de saut, c’est-à-dire en appuyant sur les talons et en se penchant légèrement en avant. Enfin, il faut sauter en appuyant sur les talons et en gardant les genoux fléchis. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds");
        $trick7->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick7);

        $trick8 = new Trick();
        $trick8->addTag($tag1);
        $trick8->addTag($tag4);
        $trick8->addTag($tag2);
        $trick8->setUser($userUser1);
        $trick8->setName('Ollie3');
        $trick8->setSlug('ollie3');
        $trick8->setDescription("L’Ollie est une figure de base qui consiste à sauter en l’air avec son snowboard. Pour réaliser un Ollie, il faut se placer en position de départ, c’est-à-dire avec les pieds collés l’un à l’autre, les genoux légèrement fléchis et le dos bien droit. Ensuite, il faut se mettre en position de saut, c’est-à-dire en appuyant sur les talons et en se penchant légèrement en avant. Enfin, il faut sauter en appuyant sur les talons et en gardant les genoux fléchis. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds");
        $trick8->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick8);

        $trick9 = new Trick();
        $trick9->addTag($tag1);
        $trick9->addTag($tag4);
        $trick9->addTag($tag2);
        $trick9->setUser($userUser1);
        $trick9->setName('Ollie4');
        $trick9->setSlug('ollie4');
        $trick9->setDescription("L’Ollie est une figure de base qui consiste à sauter en l’air avec son snowboard. Pour réaliser un Ollie, il faut se placer en position de départ, c’est-à-dire avec les pieds collés l’un à l’autre, les genoux légèrement fléchis et le dos bien droit. Ensuite, il faut se mettre en position de saut, c’est-à-dire en appuyant sur les talons et en se penchant légèrement en avant. Enfin, il faut sauter en appuyant sur les talons et en gardant les genoux fléchis. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds");
        $trick9->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick9);

        $trick10 = new Trick();
        $trick10->addTag($tag1);
        $trick10->addTag($tag4);
        $trick10->addTag($tag2);
        $trick10->setUser($userUser1);
        $trick10->setName('Ollie5');
        $trick10->setSlug('ollie5');
        $trick10->setDescription("L’Ollie est une figure de base qui consiste à sauter en l’air avec son snowboard. Pour réaliser un Ollie, il faut se placer en position de départ, c’est-à-dire avec les pieds collés l’un à l’autre, les genoux légèrement fléchis et le dos bien droit. Ensuite, il faut se mettre en position de saut, c’est-à-dire en appuyant sur les talons et en se penchant légèrement en avant. Enfin, il faut sauter en appuyant sur les talons et en gardant les genoux fléchis. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds soient bien collés l’un à l’autre. Pour que l’Ollie soit réussi, il faut que le snowboard soit bien droit et que les pieds");
        $trick10->setVideo('<iframe width="560"height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SlhGVnFPTDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>');

        $manager->persist($trick10);


        $trickImage1 = new TrickImages();
        $trickImage1->setName('bf322f8ed79729d5ab969a1c072610f8.jpg');
        $trickImage1->setTrick($trick1);
        $manager->persist($trickImage1);

        $trickImage2 = new TrickImages();
        $trickImage2->setName('1af826407889a03f9e1e5e17e3d4b011.jpg');
        $trickImage2->setTrick($trick1);
        $manager->persist($trickImage2);

        $trickImage3 = new TrickImages();
        $trickImage3->setName('44bf455824123f938546bcd79de0e7eb.jpg');
        $trickImage3->setTrick($trick1);
        $manager->persist($trickImage3);

        $trickImage4 = new TrickImages();
        $trickImage4->setName('1b08289b22c98a64d89d02d06140dafd.jpg');
        $trickImage4->setTrick($trick2);
        $manager->persist($trickImage4);

        $trickImage5 = new TrickImages();
        $trickImage5->setName('ce0fb0d1b3898a1eef2b8c83e7490ce2.webp');
        $trickImage5->setTrick($trick2);
        $manager->persist($trickImage5);

        $trickImage6 = new TrickImages();
        $trickImage6->setName('ba6146bb5fb234344b6552e1706c2442.jpg');
        $trickImage6->setTrick($trick2);
        $manager->persist($trickImage6);

        $trickImage7 = new TrickImages();
        $trickImage7->setName('14723221d727dc9cb25cb2ee4898950b.jpg');
        $trickImage7->setTrick($trick3);
        $manager->persist($trickImage7);

        $trickImage8 = new TrickImages();
        $trickImage8->setName('1e545b215861f6fdd8be260704518db5.avif');
        $trickImage8->setTrick($trick3);
        $manager->persist($trickImage8);

        $trickImage9 = new TrickImages();
        $trickImage9->setName('e9cecd6784ecc1d77de1c30a40c7df70.avif');
        $trickImage9->setTrick($trick3);
        $manager->persist($trickImage9);

        $trickImage10 = new TrickImages();
        $trickImage10->setName('5a7636e11f7445357c52d2b461b6bdaf.avif');
        $trickImage10->setTrick($trick4);
        $manager->persist($trickImage10);

        $trickImage11 = new TrickImages();
        $trickImage11->setName('fe17579f5292f5bf84853157e934bdb8.webp');
        $trickImage11->setTrick($trick4);
        $manager->persist($trickImage11);

        $trickImage12 = new TrickImages();
        $trickImage12->setName('342a283fa03d0522667868ee65fbeb3d.jpg');
        $trickImage12->setTrick($trick4);
        $manager->persist($trickImage12);

        $trickImage13 = new TrickImages();
        $trickImage13->setName('018b268151ec204dce7146b951e0f857.jpg');
        $trickImage13->setTrick($trick5);
        $manager->persist($trickImage13);

        $trickImage14 = new TrickImages();
        $trickImage14->setName('da73e63dac67e82ece2324fb22b37d32.jpg');
        $trickImage14->setTrick($trick5);
        $manager->persist($trickImage14);

        $trickImage15 = new TrickImages();
        $trickImage15->setName('b8863cbba44efbb9ae1f18a26a83a439.jpg');
        $trickImage15->setTrick($trick5);
        $manager->persist($trickImage15);

        $trickImage16 = new TrickImages();
        $trickImage16->setName('b8863cbba44efbb9ae1f18a26a83a439.jpg');
        $trickImage16->setTrick($trick6);
        $manager->persist($trickImage16);

        $trickImage17 = new TrickImages();
        $trickImage17->setName('b8863cbba44efbb9ae1f18a26a83a439.jpg');
        $trickImage17->setTrick($trick7);
        $manager->persist($trickImage17);

        $trickImage18 = new TrickImages();
        $trickImage18->setName('b8863cbba44efbb9ae1f18a26a83a439.jpg');
        $trickImage18->setTrick($trick8);
        $manager->persist($trickImage18);

        $trickImage19 = new TrickImages();
        $trickImage19->setName('b8863cbba44efbb9ae1f18a26a83a439.jpg');
        $trickImage19->setTrick($trick9);
        $manager->persist($trickImage19);

        $trickImage20 = new TrickImages();
        $trickImage20->setName('b8863cbba44efbb9ae1f18a26a83a439.jpg');
        $trickImage20->setTrick($trick10);
        $manager->persist($trickImage20);


        $comment1 = new Comment();
        $comment1->setAuthor($userUser1);
        $comment1->setTrick($trick1);
        $comment1->setContent('Superbe trick !');
        $manager->persist($comment1);

        $comment2 = new Comment();
        $comment2->setAuthor($userUser2);
        $comment2->setTrick($trick1);
        $comment2->setContent('Très belle figure !');
        $manager->persist($comment2);

        $comment3 = new Comment();
        $comment3->setAuthor($userUser1);
        $comment3->setTrick($trick1);
        $comment3->setContent('Superbe trick !');
        $manager->persist($comment3);

        $comment4 = new Comment();
        $comment4->setAuthor($userUser2);
        $comment4->setTrick($trick1);
        $comment4->setContent('Très belle figure !');
        $manager->persist($comment4);

        $comment5 = new Comment();
        $comment5->setAuthor($userUser1);
        $comment5->setTrick($trick1);
        $comment5->setContent('Superbe trick !');
        $manager->persist($comment5);

        $comment6 = new Comment();
        $comment6->setAuthor($userUser2);
        $comment6->setTrick($trick1);
        $comment6->setContent('Très belle figure !');
        $manager->persist($comment6);

        $comment7 = new Comment();
        $comment7->setAuthor($userUser1);
        $comment7->setTrick($trick1);
        $comment7->setContent('Superbe trick !');
        $manager->persist($comment7);

        $comment8 = new Comment();
        $comment8->setAuthor($userUser2);
        $comment8->setTrick($trick1);
        $comment8->setContent('Très belle figure !');
        $manager->persist($comment8);

        $comment9 = new Comment();
        $comment9->setAuthor($userUser1);
        $comment9->setTrick($trick1);
        $comment9->setContent('Superbe trick !');
        $manager->persist($comment9);

        $comment10 = new Comment();
        $comment10->setAuthor($userUser2);
        $comment10->setTrick($trick1);
        $comment10->setContent('Très belle figure !');
        $manager->persist($comment10);

        $comment11 = new Comment();
        $comment11->setAuthor($userUser1);
        $comment11->setTrick($trick1);
        $comment11->setContent('Superbe trick !');
        $manager->persist($comment11);

        $comment12 = new Comment();
        $comment12->setAuthor($userUser2);
        $comment12->setTrick($trick1);
        $comment12->setContent('Très belle figure !');
        $manager->persist($comment12);














        $manager->flush();


    }

    public static function getGroups(): array
    {
        return ['tricks'];
    }
}
