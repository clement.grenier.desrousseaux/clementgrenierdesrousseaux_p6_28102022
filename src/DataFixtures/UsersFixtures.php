<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\UserImage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;

class UsersFixtures extends Fixture implements FixtureGroupInterface
{
    private $container;

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

//        $encoder = $this->container->get('security.password_encoder');

        $userImageAdmin = new UserImage();
        $userImageAdmin->setName('81ed4858fe795952810d7ed21ec10283.webp');
        $manager->persist($userImageAdmin);

        $userImage1 = new UserImage();
        $userImage1->setName('1175125f961f08bc2609ce3ce1ccbf1b.webp');
        $manager->persist($userImage1);

        $userImage2 = new UserImage();
        $userImage2->setName('53afcf1e7d3abbd832bf25f800b974e7.webp');
        $manager->persist($userImage2);

        $userAdmin = new User();
        $userAdmin->setEmail('clement@test.admin');
//        $encodedPasswordAdmin = $encoder->encodePassword($userAdmin, 'test1234');
        $userAdmin->setPassword($userAdmin->hashPassword('test1234'));
        $userAdmin->setRoles(['ROLE_ADMIN']);
        $userAdmin->setImage($userImageAdmin);
        $userAdmin->setImage($userImageAdmin);
        $userAdmin->setUsername('Admin');
        $userAdmin->setIsVerified(true);
        $manager->persist($userAdmin);

        $userUser1 = new User();
        $userUser1->setEmail('david@test.user');
//        $encodedPasswordUser = $encoder->encodePassword($userAdmin, 'test1234');
        $userUser1->setPassword($userUser1->hashPassword('test1234'));
        $userUser1->setRoles(['ROLE_USER']);
        $userUser1->setImage($userImage1);
        $userUser1->setUsername('David');
        $userUser1->setIsVerified(true);
        $manager->persist($userUser1);


        $userUser2 = new User();
        $userUser2->setEmail('laeticia@test.user');
//        $encodedPasswordUser = $encoder->encodePassword($userAdmin, 'test1234');
        $userUser2->setPassword($userUser2->hashPassword('test1234'));
        $userUser2->setRoles(['ROLE_USER']);
        $userUser2->setImage($userImage2);
        $userUser2->setUsername('Laeticia');
        $userUser2->setIsVerified(true);
        $manager->persist($userUser2);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['users'];
    }
}
