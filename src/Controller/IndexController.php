<?php

namespace App\Controller;

use App\Repository\TrickRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function index(TrickRepository $trickRepository): Response
    {

        $allTricks = $trickRepository->findAll();
        $allTricksLenght = count($allTricks);

        $tricks = $trickRepository->findBy([], ['id' => 'DESC'], 10, 0);

        return $this->render('index/index.html.twig', [
            'tricks' => $tricks,
            'allTricksLenght' => $allTricksLenght
        ]);
    }

    /**
     * @Route("/get/tricks", name="app_get_tricks")
     */
    public function getTricks(TrickRepository $trickRepository): JsonResponse
    {
        $tricks = $trickRepository->findBy([], ['id' => 'DESC'], null, 10);

        $tricksToSend = [];
        $userToSend = [];
        $actualUserIdentifier = $this->getUser()->getUserIdentifier();
        $userToSend['actualUser'] = $actualUserIdentifier;
        $actualUserRoles = $this->getUser()->getRoles();
        $userToSend['actualUserRoles'] = $actualUserRoles;
        $user['user'] = $userToSend;
        foreach ($tricks as $trick) {
            $oneTrickToSend = [];

            $trickName = $trick->getName();
            $oneTrickToSend['name'] = $trickName;

            $trickSlug = $trick->getSlug();
            $oneTrickToSend['slug'] = $trickSlug;

            $trickId = $trick->getId();
            $oneTrickToSend['trickId'] = $trickId;

            $trickImage = $trick->getImages()[0]->getName();
            $oneTrickToSend['image'] = $trickImage;

            $trickUser = $trick->getUser();
            $oneTrickToSend['id'] = $trickUser->getUserIdentifier();

            $trickToSend[] = $oneTrickToSend;

        }
        $product[] = $trickToSend;

        $message['user'] = $user;
        $message['tricks'] = $product;

        return new JsonResponse($message);
    }
}
