<?php

namespace App\Controller;

use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class UserDeleteCommentController extends AbstractController
{

    private \Doctrine\ORM\EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/user/delete/comment/{id}', name: 'app_user_delete_comment')]
    public function index(int $id, CommentRepository $commentRepository, Security $security): Response
    {
        $commentToDelete = $commentRepository->find($id);
        $userConnected = $security->getUser();

        $commentTrickSlug = $commentToDelete->getTrick()->getSlug();

        if (!$userConnected instanceof \Symfony\Component\Security\Core\User\UserInterface) {
            $this->addFlash('warning', 'Vous n\'êtes pas connecté');

            return $this->redirectToRoute('app_one_trick', ['slug' => $commentTrickSlug]);
        }

        if ($userConnected->getRoles()[0] == "ROLE_ADMIN") {
            $this->entityManager->remove($commentToDelete);
            $this->entityManager->flush();
            $this->addFlash('success', 'Message supprimé par un administrateur');
            return $this->redirectToRoute('app_one_trick', ['slug' => $commentTrickSlug]);
        }

        if ($commentToDelete->getAuthor()->getUserIdentifier() !== $userConnected->getUserIdentifier()) {
            $this->addFlash('error', 'utilisateur différent');
            return $this->redirectToRoute('app_one_trick', ['slug' => $commentTrickSlug]);
        }

        $this->entityManager->remove($commentToDelete);
        $this->entityManager->flush();

        $this->addFlash('success', 'Message supprimé');

        return $this->redirectToRoute('app_one_trick', ['slug' => $commentTrickSlug]);


        return $this->render('user_delete_comment/index.html.twig', [
            'roles' => $userConnected->getRoles()
        ]);
    }
}
