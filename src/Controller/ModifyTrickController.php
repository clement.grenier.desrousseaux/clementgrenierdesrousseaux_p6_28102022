<?php

namespace App\Controller;

use App\Entity\Trick;
use App\Form\TrickModifyFormType;
use App\Repository\TrickRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ModifyTrickController extends AbstractController
{
    private EntityManager $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/modify/trick/{slug}', name: 'app_modify_trick')]
    public function index(string $slug, Request $request, TrickRepository $trickRepository): Response
    {

        $trickToModify = $trickRepository->findOneBy(['slug' => $slug]);

        $form = $this->createForm(TrickModifyFormType::class, $trickToModify);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $slugify = new Slugify();
            $trickToModify->setSlug($slugify->slugify($trickToModify->getName()));
            $this->entityManager->flush();

            return $this->redirectToRoute('app_one_trick', ['slug' => $slug]);
        }

        return $this->render('modify_trick/index.html.twig', [
            'trick_form' => $form->createView()
        ]);
    }
}
