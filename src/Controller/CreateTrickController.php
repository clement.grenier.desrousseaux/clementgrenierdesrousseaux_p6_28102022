<?php

namespace App\Controller;

use App\Entity\Trick;
use App\Entity\TrickImages;
use App\Form\TrickCreateFormType;
use App\Repository\TrickImagesRepository;
use App\Repository\TrickRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function PHPUnit\Framework\isNull;

class CreateTrickController extends AbstractController
{
    private \Doctrine\ORM\EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/create-trick", name="app_admin_create_trick")
     */
    public function index(Request $request, TrickRepository $trickRepository): Response
    {
        $trick = new Trick();

        $form = $this->createForm(TrickCreateFormType::class, $trick);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $slugify = new Slugify();
            $trickName = $trick->getName();
            $trickSlug = $slugify->slugify($trickName);
            $trick->setSlug($trickSlug);
            $trick->setUser($this->getUser());

            $isTrickAlreadyExists = $trickRepository->findOneBy(['slug' => $trickSlug]);

            if (!is_null($isTrickAlreadyExists)) {
                $this->addFlash('error', 'Le trick existes déjà');
                $this->redirectToRoute('app_admin_create_trick');
            } else {


                $images = $form->get('images')->getData();

                // On boucle sur les images
                foreach ($images as $image) {
                    // On génère un nouveau nom de fichier
                    $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                    // On copie le fichier dans le dossier uploads
                    $image->move(
                        $this->getParameter('images_directory'),
                        $fichier
                    );

                    // On stocke l'image dans la base de données (son nom)
                    $img = new TrickImages();
                    $img->setName($fichier);

                    $img->setTrick($trick);
                    $this->entityManager->persist($img);
                }


                $this->entityManager->persist($trick);
                $this->entityManager->flush();


                $this->addFlash('success', 'Le trick a bien été créé');

                return $this->redirectToRoute('app_index');
            }
        }

        return $this->render('admin_create_trick/index.html.twig', [
            'trick_form' => $form->createView()
        ]);


    }
}
