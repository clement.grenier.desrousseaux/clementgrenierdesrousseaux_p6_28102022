<?php

namespace App\Controller;

use App\Repository\TrickRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function Sodium\add;

class DeleteTrickController extends AbstractController
{

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/delete/trick/{id}', name: 'app_delete_trick')]
    public function index(int $id, TrickRepository $trickRepository): Response
    {
        $trickToDelete = $trickRepository->find($id);
        $this->entityManager->remove($trickToDelete);
        $this->entityManager->flush();

        $this->addFlash('success', 'Le trick a bien été supprimé');
        return $this->redirectToRoute('app_index');

    }
}
