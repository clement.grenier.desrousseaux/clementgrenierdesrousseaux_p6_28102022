<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentFormType;
use App\Repository\CommentRepository;
use App\Repository\TrickImagesRepository;
use App\Repository\TrickRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class OneTrickController extends AbstractController
{
    private \Doctrine\ORM\EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/trick/details/{slug}", name="app_one_trick")
     */
    public function index(string $slug, TrickRepository $trickRepository, Request $request, UserRepository $userRepository, CommentRepository $commentRepository, Security $security): Response
    {
        $trickToDisplay = $trickRepository->findOneBy(['slug' => $slug]);

        $trickVideos = explode("\n", $trickToDisplay->getVideo());

        $trickId = $trickToDisplay->getId();

        $userWhoComments = $security->getUser();


        $page = $request->query->getInt('page', 1); // Numéro de page par défaut : 1
        $limit = 10; // Nombre de produits à afficher par page
        $offset = ($page - 1) * $limit; // Calcul de l'offset à partir du numéro de page

        $comments = $commentRepository->findBy(['trick' => $trickId], ['id' => 'DESC']);


        $commentsToDisplay = array_slice($comments, $offset, $limit);

        $maxPagesComments = ceil(count($comments) / $limit);


        $comment = new Comment();

        $form = $this->createForm(CommentFormType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setAuthor($userWhoComments);
            $comment->setTrick($trickToDisplay);

            $this->entityManager->persist($comment);
            $this->entityManager->flush();

            $this->addFlash('success', 'Commentaire créé');
            return $this->redirectToRoute('app_one_trick', ['slug' => $slug]);
        }


        return $this->render('one_trick/index.html.twig', [
            "trick" => $trickToDisplay,
            "comment_form" => $form->createView(),
            "comments" => $commentsToDisplay,
            'user' => $userWhoComments,
            'maxPagesComments' => $maxPagesComments,
            'page' => $page,
            'trickvideo' => $trickVideos
        ]);
    }

    /**
     * @Route("/get/tricks/{slug}/images", name="app_get_tricks_images")
     */
    public function getTricks(TrickRepository $trickRepository, string $slug): JsonResponse
    {
        $trick = $trickRepository->findOneBy(['slug' => $slug]);
        $trickImages = $trick->getImages();
        $trickVideos = explode("\n", $trick->getVideo());
        $trickToSend = [];
        $user['userRole'] = $this->getUser()->getRoles();
        $user['userEmail'] = $this->getUser()->getUserIdentifier();
        $user['trickAuthor'] = $trick->getUser()->getUserIdentifier();


        foreach ($trickImages as $image) {
            $oneTrickToSend = [];

            $image->getName();
            $oneTrickToSend['imageName'] = $image->getName();

            $trickToSend[] = $oneTrickToSend;
        }

        foreach ($trickVideos as $video) {

            $oneTrickToSend = [];

            $oneTrickToSend['videoName'] = $video;


            $trickToSend[] = $oneTrickToSend;
        }

        $message['tricks'] = $trickToSend;
        $message['user'] = $user;
        return new JsonResponse($message);
    }

    /**
     * @Route("/trick/detail/{slug}/delete/image/{imageName}", name="app_onetrick_deleteimagesfromtrick")
     */
    public function deleteImagesFromTrick(TrickRepository $trickRepository, string $slug, string $imageName, TrickImagesRepository $trickImagesRepository): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $trick = $trickRepository->findOneBy(['slug' => $slug]);

        $imageToDelete = $trickImagesRepository->findOneBy(['name' => $imageName]);

        $this->entityManager->remove($imageToDelete);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_one_trick', ['slug' => $slug]);
    }

    /**
     * @Route("/trick/detail/{slug}/delete/video/{videoIndex}", name="app_onetrick_deletevideosfromtrick")
     */
    public function deleteVideosFromTrick(TrickRepository $trickRepository, string $slug, int $videoIndex, EntityManagerInterface $entityManager): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $trick = $trickRepository->findOneBy(['slug' => $slug]);
        $trickVideos = explode("\n", $trick->getVideo());
        unset($trickVideos[$videoIndex]);
        $trickVideos = implode("\n", $trickVideos);
        $trick->setVideo($trickVideos);
        $entityManager->flush();


        return $this->redirectToRoute('app_one_trick', ['slug' => $slug]);
    }
}
