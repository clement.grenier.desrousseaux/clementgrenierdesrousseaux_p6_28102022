<?php

namespace App\Controller;

use App\Entity\Tags;
use App\Form\TagsCreateFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateTagsController extends AbstractController
{
    #[Route('/create/tags', name: 'app_create_tags')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {

        $tag = new Tags();

        $form = $this->createForm(TagsCreateFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $tag->setTagName($form->get('tagName')->getData());

            $entityManager->persist($tag);
            $entityManager->flush();

            return $this->redirectToRoute('app_index');
        }
        return $this->render('create_tags/index.html.twig', [
            'controller_name' => 'CreateTagsController',
            'formTagCreate' => $form->createView()
        ]);
    }
}
