
# Openclassrooms - Projet 6


## Tech Stack

**PHP > 8.0**

**Symfony**

## Documentation


If you are using Docker (wich is recommanded), you just have to do

```bash
  docker-compose build
  docker-compose up -d

  docker exec -it myapp bash
  
  composer install
  symfony serve
```

URL for website : localhost:9000

URL for phpMyadmin : localhost:8080

URL for mailer : localhost:8081



## Environment Variables

To run this project, you will need a .env.local file (copy and paste from the .env with changing variables)

**DATABASE_URL**="mysql://userName:password@database:3306/main?serverVersion=8&charset=utf8mb4" (change userName and password)

**MAILER_DSN**=smtp://maildev_docker_symfony:25






## Loading fixtures


```bash
  make LoadFixtures
```

