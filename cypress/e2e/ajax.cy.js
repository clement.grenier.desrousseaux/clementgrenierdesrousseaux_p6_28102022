describe('I can see more tricks when i click on the button', () => {
  it('passes', () => {
    cy.visit('localhost:9000')
    cy.get('#seeMoreTricks').click().wait(500)
    cy.get('#tricks').find('.oneTrick').should('have.length.greaterThan', 10)
  })
})

describe('If I click on the button #seeMoreTricks, it disappears', () => {
  it('passes', () => {
    cy.visit('localhost:9000')
    cy.get('#seeMoreTricks').click().wait(500)
    cy.get('#seeMoreTricks').should('not.exist')
  })
})
