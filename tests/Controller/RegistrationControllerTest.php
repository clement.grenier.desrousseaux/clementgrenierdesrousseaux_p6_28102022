<?php

namespace App\Tests\Controller;

use App\Controller\RegistrationController;
use App\Repository\UserRepository;
use PHPUnit\Framework\TestCase;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends WebTestCase
{
    public static function setUpBeforeClass(): void
    {
        shell_exec('make RegistrationTestFixtures');
    }

    /**
     * @test
     */
    public function ifUserTableIsEmptyTheFirstUserCreatedWillBeAnAdmin()
    {

        $client = static::createClient();

        $client->request('GET', '/inscription');

        $client->submitForm('Register', [
            'registration_form[email]' => 'clement@test.admin',
            'registration_form[plainPassword]' => 'test1234',
        ]);


        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->find(1);
        if ($testUser) {
            self::assertSame(["ROLE_ADMIN", "ROLE_USER"], $testUser->getRoles());
        }
    }

    /**
     * @test
     */
    public function allUserAfterTheFirstUserWontBeCreateAsAdmin()
    {

        $client = static::createClient();

        $client->request('GET', '/inscription');

        $client->submitForm('Register', [
            'registration_form[email]' => 'david@test.user',
            'registration_form[plainPassword]' => 'test1234',
        ]);


        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->find(2);
        if ($testUser) {
            self::assertSame(["ROLE_USER"], $testUser->getRoles());
        }
    }
}
