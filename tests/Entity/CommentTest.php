<?php

namespace App\Tests\Entity;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\TrickRepository;
use App\Repository\UserRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CommentTest extends WebTestCase
{

    public static function setUpBeforeClass(): void
    {
        shell_exec('make CommentsTestFixtures');
    }

    /**
     * @test
     */
    public function asAdminICanPostComment()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('clement@test.admin');

        $client->loginUser($testUser);

        $client->request('GET', '/trick/details/1');

        self::assertSelectorExists("form");


        $client->submitForm('Submit', [
            'comment_form[content]' => 'Mon Premier Commentaire',
        ]);

        $trickRepository = static::$container->get(TrickRepository::class);
        $testTrick = $trickRepository->find(1);

        $commentRepository = static::$container->get(CommentRepository::class);
        $commentJustCreated = $commentRepository->find(1);


        self::assertSame("Mon Premier Commentaire", $commentJustCreated->getContent());
        self::assertSame(1, $commentJustCreated->getTrick()->getId());
    }

    /**
     * @test
     */
    public function asUserICanPostComment()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');

        $client->loginUser($testUser);

        $client->request('GET', '/trick/details/1');

        self::assertSelectorExists("form");


        $client->submitForm('Submit', [
            'comment_form[content]' => 'Mon Second Commentaire',
        ]);

        $trickRepository = static::$container->get(TrickRepository::class);
        $testTrick = $trickRepository->find(1);

        $commentRepository = static::$container->get(CommentRepository::class);
        $commentJustCreated = $commentRepository->find(2);


        self::assertSame("Mon Second Commentaire", $commentJustCreated->getContent());
        self::assertSame(1, $commentJustCreated->getTrick()->getId());
    }

    /**
     * @test
     */
    public function asVisitorICantPostAComment()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/trick/details/1');
        $this->assertResponseIsSuccessful();
        self::assertSelectorNotExists('form');
    }

//    /**
//     * @test
//     */
//    public function asAdminICanDeleteAllComments()
//    {
//        self::ensureKernelShutdown();
//        $client = static::createClient();
//        $client->followRedirects(true);
//        $userRepository = static::$container->get(UserRepository::class);
//        $testUser = $userRepository->findOneByEmail('clement@test.admin');
//
//        $client->loginUser($testUser);
//
//        $client->request('GET', '/user/delete/comment/2');
//
//        $commentRepository = static::$container->get(CommentRepository::class);
//        $commentJustCreated = $commentRepository->find(1);
//
//        self::assertNull($commentJustCreated);
//    }

    /**
     * @test
     */
    public function asUserICanDeleteMyCommentsAndOnlyMyComments()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');

        $client->loginUser($testUser);

        $client->request('GET', '/user/delete/comment/2');

        $commentRepository = static::$container->get(CommentRepository::class);
        $commentJustCreated = $commentRepository->find(2);

        self::assertNull($commentJustCreated);
    }
}
