<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Repository\TrickRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TrickTest extends WebTestCase
{

    public static function setUpBeforeClass(): void
    {
        shell_exec('make TrickTestFixtures');
    }

    /**
     * @test
     */
    public function userAdminCanCreateTrickWithSymfonyForm()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('clement@test.admin');



        $client->loginUser($testUser);

        $client->request('GET', '/admin/create-trick');

        $client->submitForm('Submit', [
            'trick_create_form[name]' => 'Mon Premier Trick',
            'trick_create_form[description]' => 'Sun super trick !',
        ]);

        $trickRepository = static::$container->get(TrickRepository::class);
        $testTrick = $trickRepository->find(1);

        self::assertSame("Mon Premier Trick", $testTrick->getName());
    }


    /**
     * @test
     */
    public function userNotAdminCantCreateTrick()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');

        $client->loginUser($testUser);

        $client->request('GET', '/admin/create-trick');
        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * @test
     */
    public function userCanModifyTrick()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');

        $client->loginUser($testUser);

        $client->request('GET', '/modify/trick/1');

        $client->submitForm('Submit', [
            'trick_modify_form[name]' => 'Mon Premier Trick Modifié',
            'trick_modify_form[description]' => 'Un super trick !',
        ]);

        $trickRepository = static::$container->get(TrickRepository::class);
        $testTrick = $trickRepository->find(1);

        self::assertSame("Mon Premier Trick Modifié", $testTrick->getName());
    }

    /**
     * @test
     */
    public function asVisitorICanSeeTricksOnHomePage()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h5', 'Mon Premier Trick Modifié');
    }

    /**
     * @test
     */
    public function userCanDeleteTrick()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');

        $client->loginUser($testUser);

        $client->request('GET', '/delete/trick/1');


        $trickRepository = static::$container->get(TrickRepository::class);
        $testTrick = $trickRepository->find(1);


        self::assertNull($testTrick);
    }

//    public static function tearDownAfterClass(): void
//    {
//        shell_exec('make database_test');
//    }

}
