document.addEventListener('animationend', event => {
    if (event.animationName === 'fadeout') {
        event.target.remove();
    }
});
