SHELL := /bin/bash

MakeMigrations:
	php bin/console make:migration
	php bin/console d:m:m

MakeMigrationsTest:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test

TrickTestFixtures:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load --group=users -n --env=test

CommentsTestFixtures:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load --group=users --group=tricks -n --env=test

RegistrationTestFixtures:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test

LoadFixturesTest:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load --group=tricks -n --env=test


LoadFixturesProd:
	symfony console doctrine:database:drop --force --env=prod || true
	symfony console doctrine:database:create --env=prod
	symfony console doctrine:migrations:migrate -n --env=prod
	symfony console doctrine:fixtures:load --group=tricks -n --env=prod


.PHONY: tests
